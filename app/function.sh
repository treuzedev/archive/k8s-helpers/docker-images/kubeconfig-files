function handler () {
  
  echo "- changing workdir to tmp" 1>&2
  cd /tmp
  
  echo "- make sure environment is empty" 1>&2
  rm -rf *
  
  echo "- getting the data" 1>&2
  EVENT_DATA=$1

  echo "- logging payload to stderr" 1>&2
  echo "$EVENT_DATA" 1>&2;
  
  echo "- extracting data from payload" 1>&2
  LB_PUBLIC_IP=$(echo $EVENT_DATA | jq -r '.lb_public_ip')
  BUCKET=$(echo $EVENT_DATA | jq -r '.bucket')
  PKI_FILES_DIR=$(echo $EVENT_DATA | jq -r '.pki_files_dir')
  KUBECONFIG_FILES_DIR=$(echo $EVENT_DATA | jq -r '.kubeconfig_files_dir')
  NUMBER_OF_PKI_FILES=$(echo $EVENT_DATA | jq -r '.number_of_pki_files')
  CLUSTER_NAME=$(echo $EVENT_DATA | jq -r '.cluster_name')
  
  echo "- checking if all necessary variables are valid" 1>&2
  case 'null' in
    $LB_PUBLIC_IP) exit 1 ;;
    $BUCKET) exit 1 ;;
    $PKI_FILES_DIR) exit 1 ;;
    $KUBECONFIG_FILES_DIR) exit 1 ;;
    $NUMBER_OF_PKI_FILES) exit 1 ;;
    $CLUSTER_NAME) exit 1 ;;
  esac
  
  echo "- preparing environment" 1>&2
  mkdir -p /tmp/files
  
  echo "- checking if pki files are available" 1>&2
  FLAG="false"
  while [[ "$FLAG" == "false" ]]
  do
    OBJECTS=$(aws s3 ls s3://$BUCKET/$PKI_FILES_DIR --recursive --summarize | grep 'Total Objects:' | awk '{print $NF}')
    if [[ "$OBJECTS" == $NUMBER_OF_PKI_FILES ]]
    then
      echo "files are ready!" 1>&2
      FLAG="true"
    else
      echo "files not ready yet.." 1>&2
      sleep 5
    fi
  done
  
  echo "- getting necessary files from s3 bucket" 1>&2
  echo "getting ca.pem" 1>&2
  aws s3 cp s3://$BUCKET/$PKI_FILES_DIR/ca.pem /tmp/files
  echo "getting kube-proxy.pem and kube-proxy-key.pem" 1>&2
  aws s3 cp s3://$BUCKET/$PKI_FILES_DIR/kube-proxy.pem /tmp/files
  aws s3 cp s3://$BUCKET/$PKI_FILES_DIR/kube-proxy-key.pem /tmp/files
  echo "getting kube-controller-manager.pem and kube-controller-manager-key.pem" 1>&2
  aws s3 cp s3://$BUCKET/$PKI_FILES_DIR/kube-controller-manager.pem /tmp/files
  aws s3 cp s3://$BUCKET/$PKI_FILES_DIR/kube-controller-manager-key.pem /tmp/files
  echo "getting kube-scheduler.pem and kube-scheduler-key.pem" 1>&2
  aws s3 cp s3://$BUCKET/$PKI_FILES_DIR/kube-scheduler.pem /tmp/files
  aws s3 cp s3://$BUCKET/$PKI_FILES_DIR/kube-scheduler-key.pem /tmp/files
  echo "getting admin.pem and admin.pem" 1>&2
  aws s3 cp s3://$BUCKET/$PKI_FILES_DIR/admin.pem /tmp/files
  aws s3 cp s3://$BUCKET/$PKI_FILES_DIR/admin-key.pem /tmp/files
  
  echo "- generating kubeconfig file for kube-proxy" 1>&2
  kubectl config set-cluster $CLUSTER_NAME \
    --certificate-authority=/tmp/files/ca.pem \
    --embed-certs=true \
    --server=https://${LB_PUBLIC_IP}:443 \
    --kubeconfig=/tmp/files/kube-proxy.kubeconfig
  kubectl config set-credentials system:kube-proxy \
    --client-certificate=/tmp/files/kube-proxy.pem \
    --client-key=/tmp/files/kube-proxy-key.pem \
    --embed-certs=true \
    --kubeconfig=/tmp/files/kube-proxy.kubeconfig
  kubectl config set-context default \
    --cluster=$CLUSTER_NAME \
    --user=system:kube-proxy \
    --kubeconfig=/tmp/files/kube-proxy.kubeconfig
  kubectl config use-context default --kubeconfig=/tmp/files/kube-proxy.kubeconfig
  
  echo "- generating kubeconfig file for kube-controller-manager" 1>&2
  kubectl config set-cluster $CLUSTER_NAME \
    --certificate-authority=/tmp/files/ca.pem \
    --embed-certs=true \
    --server=https://127.0.0.1:6443 \
    --kubeconfig=/tmp/files/kube-controller-manager.kubeconfig
  kubectl config set-credentials system:kube-controller-manager \
    --client-certificate=/tmp/files/kube-controller-manager.pem \
    --client-key=/tmp/files/kube-controller-manager-key.pem \
    --embed-certs=true \
    --kubeconfig=/tmp/files/kube-controller-manager.kubeconfig
  kubectl config set-context default \
    --cluster=$CLUSTER_NAME \
    --user=system:kube-controller-manager \
    --kubeconfig=/tmp/files/kube-controller-manager.kubeconfig
  kubectl config use-context default --kubeconfig=/tmp/files/kube-controller-manager.kubeconfig
  
  echo "- generating kubeconfig file for kube-scheduler" 1>&2
  kubectl config set-cluster $CLUSTER_NAME \
    --certificate-authority=/tmp/files/ca.pem \
    --embed-certs=true \
    --server=https://127.0.0.1:6443 \
    --kubeconfig=/tmp/files/kube-scheduler.kubeconfig
  kubectl config set-credentials system:kube-scheduler \
    --client-certificate=/tmp/files/kube-scheduler.pem \
    --client-key=/tmp/files/kube-scheduler-key.pem \
    --embed-certs=true \
    --kubeconfig=/tmp/files/kube-scheduler.kubeconfig
  kubectl config set-context default \
    --cluster=$CLUSTER_NAME \
    --user=system:kube-scheduler \
    --kubeconfig=/tmp/files/kube-scheduler.kubeconfig
  kubectl config use-context default --kubeconfig=/tmp/files/kube-scheduler.kubeconfig
  
  echo "- generating kubeconfig file for local admin user" 1>&2
  kubectl config set-cluster $CLUSTER_NAME \
    --certificate-authority=/tmp/files/ca.pem \
    --embed-certs=true \
    --server=https://127.0.0.1:6443 \
    --kubeconfig=/tmp/files/admin.kubeconfig
  kubectl config set-credentials admin \
    --client-certificate=/tmp/files/admin.pem \
    --client-key=/tmp/files/admin-key.pem \
    --embed-certs=true \
    --kubeconfig=/tmp/files/admin.kubeconfig
  kubectl config set-context default \
    --cluster=$CLUSTER_NAME \
    --user=admin \
    --kubeconfig=/tmp/files/admin.kubeconfig
  kubectl config use-context default --kubeconfig=/tmp/files/admin.kubeconfig
  
  echo "- generating kubeconfig file for remote admin user" 1>&2
  kubectl config set-cluster $CLUSTER_NAME \
    --certificate-authority=/tmp/files/ca.pem \
    --embed-certs=true \
    --server=https://$LB_PUBLIC_IP:443 \
    --kubeconfig=/tmp/files/remote-admin.kubeconfig
  kubectl config set-credentials remote-admin \
    --client-certificate=/tmp/files/admin.pem \
    --client-key=/tmp/files/admin-key.pem \
    --embed-certs=true \
    --kubeconfig=/tmp/files/remote-admin.kubeconfig
  kubectl config set-context default \
    --cluster=$CLUSTER_NAME \
    --user=remote-admin \
    --kubeconfig=/tmp/files/remote-admin.kubeconfig
  kubectl config use-context default --kubeconfig=/tmp/files/remote-admin.kubeconfig
  
  echo "- generating kubeconfig file for kube-router" 1>&2
  kubectl config set-cluster $CLUSTER_NAME \
    --certificate-authority=/tmp/files/ca.pem \
    --embed-certs=true \
    --server=https://$LB_PUBLIC_IP:443 \
    --kubeconfig=/tmp/files/kube-router.kubeconfig
  kubectl config set-credentials kube-router \
    --client-certificate=/tmp/files/admin.pem \
    --client-key=/tmp/files/admin-key.pem \
    --embed-certs=true \
    --kubeconfig=/tmp/files/kube-router.kubeconfig
  kubectl config set-context default \
    --cluster=$CLUSTER_NAME \
    --user=kube-router \
    --kubeconfig=/tmp/files/kube-router.kubeconfig
  kubectl config use-context default --kubeconfig=/tmp/files/kube-router.kubeconfig
  
  echo "- listing generated files" 1>&2
  ls /tmp/files
  
  echo "- copying files to s3" 1>&2
  aws s3 cp /tmp/files/admin.kubeconfig s3://$BUCKET/$KUBECONFIG_FILES_DIR/
  aws s3 cp /tmp/files/remote-admin.kubeconfig s3://$BUCKET/$KUBECONFIG_FILES_DIR/
  aws s3 cp /tmp/files/kube-router.kubeconfig s3://$BUCKET/$KUBECONFIG_FILES_DIR/
  aws s3 cp /tmp/files/kube-scheduler.kubeconfig s3://$BUCKET/$KUBECONFIG_FILES_DIR/
  aws s3 cp /tmp/files/kube-controller-manager.kubeconfig s3://$BUCKET/$KUBECONFIG_FILES_DIR/
  aws s3 cp /tmp/files/kube-proxy.kubeconfig s3://$BUCKET/$KUBECONFIG_FILES_DIR/
  
  echo "- cleaning up files" 1>&2
  rm -rf *

  echo "- returning an answer to lambda" 1>&2
  RESPONSE="PKI creation successful!"
  echo $RESPONSE
  
}
