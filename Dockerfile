FROM amazon/aws-lambda-provided:al2 as tmp

WORKDIR /var/task
COPY app/bootstrap /var/runtime
COPY app/function.sh /var/task

RUN yum upgrade -y && \
    yum install -y jq unzip && \
    curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install && \
    aws --version && \
    rm -rf awscliv2.zip aws && \
    curl -LO https://dl.k8s.io/release/v1.21.0/bin/linux/amd64/kubectl && \
    install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl && \
    kubectl version --client && \
    yum remove -y unzip && \
    yum clean all && \
    rm -rf /var/cache/yum && \
    chmod 755 /var/runtime/bootstrap /var/task/function.sh
    
FROM tmp

CMD ["function.sh.handler"]
